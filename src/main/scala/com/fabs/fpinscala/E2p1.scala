package com.fabs.fpinscala

import scala.annotation.tailrec

/**
  * Write a recursive function to get the nth Fibonacci number (http://mng.bz/C29s). The first two Fibonacci numbers are
  * 0 and 1. The nth number is always the sum of the previous two—the sequence begins 0, 1, 1, 2, 3, 5. Your definition
  * should use a local tail-recursive function.
  * def fib(n: Int): Int
  */
object E2p1 extends App {
  println(formatResult("Fibonacci", 20, fib))
  println(formatResult("Factorial", 8, fac))

  def fib(nth: Int): Int = {
    @tailrec
    def loop(n: Int, secondLast: Int, last: Int, acc: Int): Int = {
      if (n <= 0) acc
      else loop(n - 1, last, acc, last + acc)
    }

    loop(nth, 0, 1, 0)
  }

  def fac(n: Int): Int = {
    @tailrec
    def loop(counter: Int, acc: Int): Int = {
      if (counter <= 0) acc
      else loop(counter - 1, counter * acc)
    }

    loop(n, 1)
  }

  def formatResult(op: String, n: Integer, f: Int => Int): String = {
    s"The $op of $n is ${f(n)}"
  }
}
