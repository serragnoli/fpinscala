package com.fabs.fpinscala

import scala.io.Source

object LoremIpsumCounter extends App {
  val r = Source.fromFile("filesDir/otherDir/l.txt")
    .getLines()
    .map(line => line.replaceAll("[^\\w]", " "))
    .flatMap(_.split("\\s"))
    .toList
    .filterNot(_.isEmpty)
    .groupBy(word => word.toLowerCase)
    .mapValues(_.length)
    .toSeq.sortBy(_._1)

  r.foreach(println)
}
