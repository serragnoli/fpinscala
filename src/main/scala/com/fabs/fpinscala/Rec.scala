package com.fabs.fpinscala

import scala.reflect.io.{Directory, File, Path}

object Rec extends App {
  val d = Directory("/Users/fse05/Development/Learning/fpinscala/filesDir")

  d.deepList().filter(_.isFile).foreach(println)
  println("=======")
  d.deepList().filter(_.isDirectory).foreach(println)
  println("=======")
  d.deepList().filter(_.extension == "bak").foreach(println)
}
