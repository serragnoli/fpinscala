package com.fabs.fpinscala

import scala.language.implicitConversions

/**
  * Implement the function tail for removing the first element of a List. Note that the function takes constant time.
  * What are different choices you could make in your implementation if the List is Nil? We’ll return to this
  * question in the next chapter.
  */
object E3p2 extends App {

  val li = List(1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16)
  val ld = List(1, 2, 13)

  println(s"Original $li")
  println(s"Modified ${myDrop(li)}")

  println(s"Original $ld")
  println(s"Modified ${myDrop(ld)}")

  def myDrop(l: List[Int]): List[Int] = {
    l.dropWhile(_ < 10)
//    List(AnyRef)
  }
}
