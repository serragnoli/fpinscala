package com.fabs.fpinscala

import scala.annotation.tailrec

/**
  * Implement isSorted, which checks whether an Array[A] is sorted according to a given comparison function:
  * def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean
  */
object E2p2 extends App {
  println(isSorted(Array(7, 8, 1, 3), (first: Int, second: Int) => first > second))
  println(isSorted(Array(7, 8, 10, 31), (first: Int, second: Int) => first > second))
  println(isSorted(Array("James", "Fabs", "Mike", "Nina"), (first: String, second: String) => first.compare(second) > 0))
  println(isSorted(Array("Fabs", "James", "Mike", "Nina"), (first: String, second: String) => first.compare(second) > 0))

  def isSorted[A](arr: Array[A], f: (A, A) => Boolean): Boolean = {
    @tailrec
    def loop(n: Int): Boolean = {
      if (n >= arr.length - 1) true
      else if (f(arr(n), arr(n + 1))) false
      else loop(n + 1)
    }

    loop(0)
  }
}
