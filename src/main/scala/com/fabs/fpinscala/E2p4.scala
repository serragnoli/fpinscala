package com.fabs.fpinscala

/**
  * Implement uncurry, which reverses the transformation of curry. Note that since => associates to the right,
  * A => (B => C) can be written as A => B => C.
  *
  */
object E2p4 {
  def uncurry[A, B, C](f: A => B => C): (A, B) => C = {
    (a: A, b: B) => f(a)(b)
  }

  def main(args: Array[String]): Unit = {
    val sum = (a: Int) => (b: Int) => a + b
    val uncurried = uncurry[Int, Int, Int](sum)
    println("sum(1)(2)", sum(1)(2))
    println("uncurry(1,2)", uncurried(1, 2))
  }
}
