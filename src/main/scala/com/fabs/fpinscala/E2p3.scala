package com.fabs.fpinscala

/**
  * Let’s look at another example, currying, which converts a function f of two arguments into a function of one
  * argument that partially applies f. Here again there’s only one implementation that compiles. Write this implementation.
  */
object E2p3 {

  def curry[A, B, C](f: (A, B) => C): A => (B => C) = {
    a: A => b: B => f(a, b)
  }

  def main(args: Array[String]): Unit = {
    val c = curry((a: Int, b: Int) => a == b)
    println("1 == 2? ", c(1)(2))
    println("2 == 2? ", c(2)(2))

    val c_partial = c(1)

    println("[partial] 1 == 2? ", c_partial(2))
    println("[partial] 1 == 1? ", c_partial(1))
  }
}
