package com.fabs.fpinscala

object L4p1 extends App {

//  println(mean1(List()))
  println(mean2(List[Double]()))
  println(List(1,22,3,4).max)

  def mean1(xs: Seq[Double]): Double =
    if (xs.isEmpty)
      throw new ArithmeticException("Fabs dislike 0")
    else xs.sum / xs.length

  def mean2(xs: Seq[Double]): Double =
    xs.sum / xs.length
}
