package com.fabs.fpinscala

/**
  * Let’s look at a final example, function composition, which feeds the output of one function to the input of
  * another function. Again, the implementation of this function is fully determined by its type signature.
  *
  * Implement the higher-order function that composes two functions.
  */
object E2p5 {
  def compose[A, B, C](f: B => C, g: A => B): A => C = {
    a => f(g(a))
  }

  def main(args: Array[String]): Unit = {
    val double = (a: Int) => 2 * a
    val toString = (a: Int) => "new value " + a
    val c = compose(toString, double)
    println("original value 2", c(2))

    val f = (x: Double) => math.Pi / 2 - x
    println(f andThen math.sin)
  }
}
